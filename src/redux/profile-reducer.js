import { usersAPI, profileAPI } from '../api/api';
import { stopSubmit } from 'redux-form';

const ADD_POST = 'ADD-POST';
const SET_USERS_PROFILE = 'SET_USERS_PROFILE';
const SET_STATUS = 'SET_STATUS';
const SAVE_PHOTO_SUCCESS = 'SAVE_PHOTO_SUCCESS';

let initialState = {
    posts: [
        { id: 1, postText: 'Это новый пост 1', likeCount: 45 },
        { id: 2, postText: 'Это новый пост 2', likeCount: 12 },
        { id: 3, postText: 'Это новый пост 3', likeCount: 60 }
    ],
    profile: null,
    status: ""
};

const profileReducer = (state = initialState, action) => {

    switch (action.type) {

        case ADD_POST: {
            let BodyNewPost = {
                id: 4,
                postText: action.TextPost,
                likeCount: 0
            }
            return {
                ...state,
                posts: [...state.posts, BodyNewPost]
            }
        }
        case SET_USERS_PROFILE: {
            return {
                ...state, profile: action.profile
            }
        }
        case SET_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }
        case SAVE_PHOTO_SUCCESS: 
        debugger;
            return { ...state, 
            profile: {...state.profile, photos: action.photos }
        }
         
        default:
            return state;
    }
}

export const addPostActionCreator = (TextPost) => ({ type: ADD_POST,TextPost })
export const setUsersProfile = (profile) =>({ type: SET_USERS_PROFILE, profile })
export const setUserStatus = (status) => ({ type: SET_STATUS, status })
export const setPhotosSuccess  = (photos) => ({ type: SAVE_PHOTO_SUCCESS, photos })

export const getUserProfile = (userID) => async (dispatch) => {
    let Response = await usersAPI.getProfile(userID);
            dispatch(setUsersProfile(Response.data));    
}

export const getUserStatus = (userID) => async (dispatch) => {
    let Response = await profileAPI.getStatus(userID);
            dispatch(setUserStatus(Response.data)); 
}

export const updateUserStatus = (status) => async (dispatch) => {
    let Response = await profileAPI.updateStatus(status)
            if (Response.data.resultCode === 0) {
                dispatch(setUserStatus(status));
            }
    }

export const savePhoto = (file) => async (dispatch) => {
        let Response = await profileAPI.savePhoto(file)
                if (Response.data.resultCode === 0) {
                    dispatch(setPhotosSuccess(Response.data.data.photos));
                }
        }

 export const saveProfile = (profile) => async (dispatch, getState) => {
   const userId = getState().authorization.userId;

            const Response = await profileAPI.saveProfile(profile);
                    if (Response.data.resultCode === 0) {
                     dispatch(getUserProfile(userId));
                    }
                    else {
                        dispatch(stopSubmit('edit-profile', {_error: Response.data.messages[0] }));
                        return Promise.reject(Response.data.messages[0] );
                    }
            }
    
export default profileReducer;