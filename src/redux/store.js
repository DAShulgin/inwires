import dialogReducer  from './dialog-reducer'
import profileReducer from './profile-reducer';

let store = {
    _state: {
        dialogPage: {
            dialogs: [
                { id: 1, name: 'Андрей' },
                { id: 2, name: 'Валерия' },
                { id: 3, name: 'Антон' },
                { id: 4, name: 'Дмитрий' },
                { id: 5, name: 'Виктория' }
            ],
            message: [
                { id: 1, text: 'Привет' },
                { id: 2, text: 'Хай' },
                { id: 3, text: 'GG' },
                { id: 4, text: 'Какие дела' },
                { id: 5, text: 'Что нового у тебя' }
            ],
            newMessageText: 'Новое сообщение'
        },
        profilePage: {
            posts: [
                { id: 1, postText: 'Это новый пост 1', likeCount: 45 },
                { id: 2, postText: 'Это новый пост 2', likeCount: 12 },
                { id: 3, postText: 'Это новый пост 3', likeCount: 60 }
            ],
            newPostText: 'Новый пост'
        }
    },
    _callSubscriber() {
        console.log('State changed');
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer; //наблюдатель
    },

    dispatch(action) {
        this._state.dialogPage = dialogReducer(this._state.dialogPage, action);
        this._state.profilePage = profileReducer(this._state.profilePage, action)
        this._callSubscriber(this._state);     
    }
}

export default store;