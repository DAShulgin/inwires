import { authAPI, securityAPI } from '../api/api';
import { stopSubmit } from 'redux-form';

const SET_USER_DATA = 'social-network/authorization/SET_USER_DATA';
const GET_CAPTCHA_URL_SUCCESS = 'social-network/authorization/GET_CAPTCHA_URL_Succcess';



let initialState = {
    userId: null,
    email: null,
    login: null,
    isAuth: false,
    CaptchaUrl: null
};

const authorizationReducer = (state = initialState, action) => {

    switch (action.type) {

        case SET_USER_DATA:
        case GET_CAPTCHA_URL_SUCCESS: 
            return {
                ...state,
                ...action.payload,
            }      
        default:
            return state;
    }
}

export const setAuthUserData = (userId, email, login, isAuth) => ({ type: SET_USER_DATA, payload: { userId, email, login, isAuth } });
export const getCaptchaUrlSuccess = (CaptchaUrl) => ({ type: GET_CAPTCHA_URL_SUCCESS, payload: {CaptchaUrl} });


export const getAuthUserData = () => async (dispatch) => {
    const Response = await authAPI.authorization();

    if (Response.data.resultCode === 0) {
        let { id, login, email } = Response.data.data
        dispatch(setAuthUserData(id, email, login, true));
    }
}

export const login = (email, password, rememberMe, Captcha) => async (dispatch) => {
    const Response = await authAPI.login(email, password, rememberMe, Captcha);

    if (Response.data.resultCode === 0) {
        dispatch(getAuthUserData())
    }
    else {
        if (Response.data.resultCode === 10) {
            dispatch(getCaptchaUrl());   
        }
        let messages = Response.data.messages.length > 0 ? Response.data.messages[0] : 'Some error';
        dispatch(stopSubmit('login', { _error: messages }));
    }
}

export const  getCaptchaUrl = () => async (dispatch) => {
   const Response = await securityAPI.getCaptchaUrl();
    const CaptchaUrl = Response.data.url; 
    dispatch(getCaptchaUrlSuccess(CaptchaUrl));  
}


export const logout = () => async (dispatch) => {
    const Response = await  authAPI.logout();
        if (Response.data.resultCode === 0) {
            dispatch(setAuthUserData(null, null, null, false));
        }
}

export default authorizationReducer;