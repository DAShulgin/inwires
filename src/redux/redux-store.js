import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import dialogReducer  from './dialog-reducer'
import profileReducer from './profile-reducer';
import usersReducer from './users-reducer';
import authorizationReducer from  './authorization-reducer';
import thunk from 'redux-thunk';
import { reducer as formReducer } from 'redux-form'
import appReducer from "./app-reducer";

let reducers = combineReducers({
    dialogPage: dialogReducer,
    profilePage: profileReducer,  
    usersPage: usersReducer,
    authorization: authorizationReducer,
    app: appReducer,
    form: formReducer
})


 const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
 const store = createStore(reducers, composeEnhancers( applyMiddleware(thunk)));

export default store;