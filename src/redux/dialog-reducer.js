const ADD_MESSAGE = 'ADD-MESSAGE';

let initialState = {
    dialogs: [
        { id: 1, name: 'Андрей' },
        { id: 2, name: 'Валерия' },
        { id: 3, name: 'Антон' },
        { id: 4, name: 'Дмитрий' },
        { id: 5, name: 'Виктория' }
    ],
    message: [
        { id: 1, text: 'Привет' },
        { id: 2, text: 'Хай' },
        { id: 3, text: 'GG' },
        { id: 4, text: 'Какие дела' },
        { id: 5, text: 'Что нового у тебя' }
    ],
};

const dialogReducer = (state = initialState, action) => {

    switch (action.type) {

        case ADD_MESSAGE:
            let newMessage = {
                id: 6,
                text: action.newMessageText
            };
            return {
                ...state,
                message: [...state.message, newMessage]
            };
        default:
            return state;
    }
}

export const addMessage = (newMessageText) => ({ type: ADD_MESSAGE, newMessageText})

export default dialogReducer;