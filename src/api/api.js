import * as axios from 'axios';


const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0',
    headers: { 'API-KEY': 'eea7e5a2-9d5c-45b4-8e1d-5e5e4d07111d' },
});


export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 5) {
        return instance.get(`/users?page=${currentPage}&count=${pageSize}`, {
            withCredentials: true
        }).then(Response => {
            return Response.data
        });
    },
    follow(userid) {
        return instance.post(`/follow/${userid}`)
    },
    unfollow(userid) {
        return instance.delete(`/follow/${userid}`)
    },
    getProfile(userID) {
        console.warn('Obsolete method. Please profileAPI object.')
        return profileAPI.getProfile(userID);
    }
}
export const profileAPI = {
    getProfile(userID) {
        return instance.get(`/profile/` + userID)
    },
    getStatus(userID){
        return instance.get(`/profile/status/` + userID)
    },
    updateStatus(status){
        return instance.put(`/profile/status`, {status: status});
    },
    savePhoto(photoFile) {
        const formData = new FormData();
        formData.append('image', photoFile )
        return instance.put(`/profile/photo`, formData,  {
            headers: { 
                'Content-type': 'multipart/form-data'
            }
        });    
    },
    saveProfile(profile) {
        return instance.put(`profile`, profile );
    }
}

export const authAPI = {
    authorization() {
        return instance.get(`/auth/me`)
    },
    login(email,password,rememberMe = false, Captcha = null) {
        return instance.post(`/auth/login` , { email,password,rememberMe,Captcha });
    },
    logout() {
        return instance.delete(`/auth/login`);
    }
}

export const securityAPI = {
    getCaptchaUrl() {
        return instance.get(`security/get-captcha-url`)
    
    }
}