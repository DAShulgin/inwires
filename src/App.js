import React from 'react';
import './App.css';
import HeaderContainer from './Components/Header/HeaderContainer';
import Navbar from './Components/Navbar/Navbar';
import News from './Components/News/News';
import Muzik from './Components/Muzik/Muzik';
import Setting from './Components/Setting/Setting';
import UsersContainer from'./Components/Users/UsersContainer';
import Login from './Components/Login/Login';
import { connect } from 'react-redux';
import { initializeApp } from './redux/app-reducer';
import {Route , withRouter, Switch, Redirect } from 'react-router-dom';
import { compose } from '../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/redux';
import Preloader from './Components/common/Preloader/Preloader';
import { withSuspense } from './Components/hoc/withSuspense';


const DialogsContainer = React.lazy(() => import('./Components/Dialogs/DialogsContainer'));
const ProfileContainer = React.lazy(() => import('./Components/Profile/ProfileContainer'));


class App extends React.Component {

  componentDidMount() {
    this.props.initializeApp();
  }

  render() {
    if (!this.props.initialized)
      return <Preloader />

    return (

      <div className='app-INwires'>
        <HeaderContainer />
        <Navbar />
        <div className="app-INwires-content">
        <Switch>

          <Route exact path='/'><Redirect  to='/profile' /></Route> 

          <Route path='/profile/:userId?' render={ withSuspense(ProfileContainer) } />
          <Route path='/dialogs' render={ withSuspense(DialogsContainer) } />

          <Route path='/users' render={() => <UsersContainer />} />
          <Route path='/login' render={() => <Login />} />

          <Route path='/news' render={() => <News />} />
          <Route path='/muzik' render={() => <Muzik />} />
          <Route path='/setting' render={() => <Setting />} />
         
          <Route path='*' render={() => <div><b><h1>404 NOT FOUND</h1></b> </div>} />

          </Switch>
        </div>
      </div>
    )
  }
}

let mapStateToProps = (state) => ({
  initialized: state.app.initialized,
});

export default compose(
  withRouter,
  connect(mapStateToProps, { initializeApp }))(App); 
