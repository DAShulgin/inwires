import React from 'react';
import { connect } from 'react-redux';
import { follow, unfollow,  setCurrentPage, requestUsers } from '../../redux/users-reducer';
import Users from './Users';
import Preloader from '../common/Preloader/Preloader';
import { compose } from '../../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/redux';
import {getUsersSuperSelector, getPageSize, getTotalUsersCount, getCurrentPage, getIsFetching, getFollowinInProgress } from '../../redux/users-selectors';

class UsersContainer extends React.Component {

    componentDidMount() {
    const {currentPage, pageSize} = this.props;
    this.props.requestUsers(currentPage, pageSize);
    }
    onPageChanged = (pageNumber) => {
    const pageSize = this.props;   
    this.props.setCurrentPage(pageNumber);
    this.props.requestUsers(pageNumber, pageSize);    
    }
    render() {
        return <>
            {this.props.isFetching ? <Preloader /> : null}
            <Users
                totalUsersCount={this.props.totalUsersCount}
                pageSize={this.props.pageSize}
                currentPage={this.props.currentPage}
                onPageChanged={this.onPageChanged}
                users={this.props.users}
                unfollow={this.props.unfollow}
                follow={this.props.follow}
                togglefollowinProgress = {this.props.togglefollowinProgress}
                followinInProgress= {this.props.followinInProgress}
            />
        </>
    }
}

let mapStateToProps = (state) => {
    return {
        users: getUsersSuperSelector(state),
        pageSize: getPageSize(state),
        totalUsersCount: getTotalUsersCount(state) ,
        currentPage: getCurrentPage(state),
        isFetching: getIsFetching(state),
        followinInProgress: getFollowinInProgress(state)
    }
} 

export default compose (
connect( mapStateToProps, {follow, unfollow, setCurrentPage, requestUsers })) (UsersContainer);

