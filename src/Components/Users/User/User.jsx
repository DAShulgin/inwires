import React from 'react';
import styles from './User.module.css';
import notUser from '../../../Images/notUser.png';
import { NavLink } from "react-router-dom";


let User = ({ user , followinInProgress, unfollow, follow }) => {

    return (
        <div>
            <div className={styles.blok}>
                <div className={styles.fullname}>{user.name}</div>
                <div className={styles.status}> {user.status != null ? user.status : "Статус не установлен"}</div>
                <div><NavLink to={'/profile/' + user.id}>
                    <img src={user.photos.small != null ? user.photos.small : notUser} className={styles.UsersPhoto} />
                </NavLink></div>
                <div className={styles.follow} >
                    {user.followed
                        ? <button disabled={followinInProgress.some(id => id === user.id)} onClick={() => { unfollow(user.id) }}>Отписаться </button>
                        : <button disabled={followinInProgress.some(id => id === user.id)} onClick={() => { follow(user.id) }}>Подписаться </button>
                    }
                </div>
                <div>{"u.location.contry"}</div>
                <div>{"u.location.city"}</div>

            </div>
        </div>
    )
}

export default User;