import React from 'react';
import styles from './Users.module.css';
import User from './User/User';
import Paginator from '../common/Paginator/Paginator';

let Users = ({ currentPage, onPageChanged, totalUsersCount, pageSize, users, ...props }) => {

    return <div>
        <div className = {styles.positionPaginator}><Paginator currentPage={currentPage} onPageChanged={onPageChanged}
            totalItemsCount={totalUsersCount} pageSize={pageSize} />
        </div>
        <div className={styles.item}>
            {
                users.map(u =>
                    <User key={u.id} user={u}
                        followinInProgress={props.followinInProgress}
                        unfollow={props.unfollow}
                        follow={props.follow}
                    />
                )
            }
        </div>
    </div>

}


export default Users;