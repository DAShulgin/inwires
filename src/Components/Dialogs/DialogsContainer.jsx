import React from 'react';
import Dialogs from './Dialogs';
import { addMessage } from '../../redux/dialog-reducer';
import { connect } from 'react-redux';
import { withAuthRedirect } from '../hoc/withAuthRedirect';
import { compose } from '../../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/redux';


const mapStateToProps = (state) => {
    return {
        dialogPage: state.dialogPage,
    }
}

export default compose(
    connect(mapStateToProps, { addMessage }),
    withAuthRedirect
)(Dialogs);
