import React from 'react';
import style from './Dialogs.module.css';
import { NavLink } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form'
import { maxLengthCreator, required } from '../../utils/validators/validator';
import { Textarea } from '../common/FormControls/FormControls';



const Dialogs = (props) => {

    let state = props.dialogPage;

    const DialogItem = (props) => {
        return (
            <div className={style.active}>
                <NavLink to={"/dialogs/" + props.id}>{props.name}</NavLink>
            </div>
        );
    }
    const MessageItem = (props) => {
        return (
            <div>{props.message}</div>
        );
    }

    let dialogsElements = state.dialogs
        .map(d => <DialogItem key={d.id} id={d.id} name={d.name} />);

    let messageElements = state.message
        .map(m => <MessageItem key={m.id} id={m.id} message={m.text} />)
    
        const addNewMessage = (values) => {
        props.addMessage(values.newMessageText);
        }

    return (
        <div className={style.content} >
            <div className={style.block1}>
                {dialogsElements}
            </div>
            <div className={style.block2}>
                <div> {messageElements}</div>
                <div>
                    <AddMessageReduxForm  onSubmit = {addNewMessage} />
                </div>
            </div>
        </div>
    )
}

const maxLength10 = maxLengthCreator(25); 

const AddMessageForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={Textarea} name={'newMessageText'} placeholder={'Введите сообщение'}  validate={ [required, maxLength10]}  />
            </div>
            <div> <button>Отправить сообщение</button> </div>
        </form>
    )
}

const AddMessageReduxForm = reduxForm({ form: 'dialogAddMessageForm' })(AddMessageForm);


export default Dialogs;