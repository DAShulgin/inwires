import React from 'react';
import style from './Login.module.css';
import { reduxForm } from 'redux-form'
import {login } from '../../redux/authorization-reducer';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import s from '../common/FormControls/FormControls.module.css';
import {Input, createField} from  '../common/FormControls/FormControls'
import {required, maxLengthCreator } from '../../utils/validators/validator';


const maxLength25 = maxLengthCreator(25);

const LoginForm = ( {handleSubmit, error, CaptchaUrl}) => {
    return (
        <form  onSubmit = {handleSubmit}>

             { createField('Email', 'email', Input , [required, maxLength25]) } 
             { createField('Password', 'password', Input,  [required, maxLength25], {type:'password'} )}
             { createField(null, 'rememberMe', 'Input', [], {type:'checkbox'}, 'remember me' )}

           { CaptchaUrl && <img src= {CaptchaUrl} /> }
           { CaptchaUrl &&  createField('Введите символы с картинки', 'Captcha', Input , [required], {}) }
           
           { error && <div className={s.formSummaryError} > {error} </div> }
            <div>
                <button>Вход</button>
            </div>
        </form>
    )
}

const LoginReduxForm = reduxForm({ form: 'login' })(LoginForm)

const Login = (props) => {
    const onSubmit = (FormData) => {
     props.login(FormData.email, FormData.password, FormData.rememberMe, FormData.Captcha)
    }

    if (props.isAuth) {
        return <Redirect to= {'/profile'} />
    }

    return (
        <div className={style.blok} >
            <div className={style.position} >
                <h1>Авторизация</h1>
                <LoginReduxForm  onSubmit = {onSubmit} CaptchaUrl = {props.CaptchaUrl}/>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        CaptchaUrl: state.authorization.CaptchaUrl,
        isAuth: state.authorization.isAuth
    }
  }

export default connect(mapStateToProps, {login}) (Login);