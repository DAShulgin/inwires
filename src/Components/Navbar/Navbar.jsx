import React from 'react';
import s from './Navbar.module.css';
import { NavLink } from 'react-router-dom';
import muzik from '../../Images/muzik.png'
import dialogs from '../../Images/dialogs.png';
import setting from '../../Images/setting.png';
import user from '../../Images/user.png';
import users from '../../Images/users.png';
import news from '../../Images/news.png';

const Navbar = () => {
    return (
        <nav className={s.navigation}>

            <div className={s.menu}>
                <div>
                    <NavLink to='/profile' activeClassName={s.active}>
                        <img src={user} />
                        Профиль </NavLink>
                </div>
                <div>
                    <NavLink to='/dialogs' activeClassName={s.active} >
                        <img src={dialogs} />
                        Сообщения </NavLink>
                </div>
                <div >
                    <NavLink to='/users' activeClassName={s.active} >
                        <img src={users} />
                        Пользователи</NavLink>
                </div>
                <div>
                    <NavLink to='/muzik' activeClassName={s.active} >
                        {<img src={muzik} />}
                        Музыка</NavLink>
                </div>
                <div >
                    <NavLink to='/news' activeClassName={s.active} >
                        <img src={news} />
                        Новости</NavLink>
                </div>
                <div >
                    <NavLink to='/setting' activeClassName={s.active}>
                        <img src={setting} />
                        Настройки</NavLink>
                </div>
            </div>
        </nav >
    );
}

export default Navbar;