import React, { useState } from 'react';
import style from './ProfileInfo.module.css';
import notUser from '../../../Images/notUser.png';
import Preloader from '../../common/Preloader/Preloader';
import ProfileStatusWithHooks from './ProfileStatus/ProfileStatusWithHooks';
import ProfileData from './ProfileData/ProfileData';
import ProfileDataForm from './ProfileDataForm/ProfileDataForm';

const Profileinfo = ({ profile, status, updateUserStatus, isOwner, savePhoto, saveProfile }) => {

  let [editMode, setEditMode] = useState(false);

  if (!profile) {
    return <Preloader />
  }

  const onMainphotoSelected = (e) => {
    if (e.target.files.length)
      savePhoto(e.target.files[0]);
  }

  const onSubmit = (formData) => {
    saveProfile(formData).then(
      () => {
        setEditMode(false);
      }
    );
    return true;
  }

  return (
    <div className={style.content} >
      <div className={style.block1}>
        <div><img src={profile.photos.large != null ? profile.photos.large : notUser} /> </div>
        <div>{isOwner && <input type={'file'} onChange={onMainphotoSelected} />} </div>
      </div>
      <div className={style.block2}>
        <div className={style.contentInfo}>
          <div className={style.contentInfo_block1}>
            <div>{profile.fullName}</div>
            <div><ProfileStatusWithHooks status={status} updateUserStatus={updateUserStatus} /></div>
          </div>
          <div className={style.contentInfo_block2}>
            {editMode ? <ProfileDataForm initialValues={profile} profile={profile} onSubmit={onSubmit} /> : <ProfileData profile={profile} isOwner={isOwner} goToEditMode={() => setEditMode(true)} />}
          </div>
        </div>
      </div>
    </div>
  )
}

export default Profileinfo;
