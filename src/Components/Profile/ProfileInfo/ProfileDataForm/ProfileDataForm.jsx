import React from 'react';
import { createField, Input, Textarea } from '../../../common/FormControls/FormControls';
import { reduxForm } from 'redux-form';
import s from '../../../common/FormControls/FormControls.module.css';

const ProfileDataForm = ({handleSubmit,  profile, error }) => {
    return <form onSubmit = {handleSubmit}>
        <div><button>Save</button></div>
        { error && <div className={s.formSummaryError} > {error} </div> }
        <div> <b>Полное имя: </b> {createField('Введите имя', 'fullName', Input, [])} </div>
        <div> <b>Ищу работу: </b> {createField('', 'lookingForAJob', Input, [], { type: 'checkbox' })}
        </div>
        <div>
            <b>My profession skills: </b> {profile.lookingForAJobDescription}
            {createField('Мои профессиональные навыки', 'lookingForAJobDescription', Textarea, [] ) }
        </div>
        <div> <b>About me: </b> {profile.aboutMe}
        {createField('aboutMe', 'aboutMe', Textarea, [] ) }
        </div>
         <div> 
             <b>Contacts: </b> {Object.keys(profile.contacts).map(key => {
            return  <div key ={key}> <b>  {key}: { createField(key, 'contacts.' + key, Input, [])}  </b>
            </div>       
        })}
        </div> 
    </form>
}

const ProfileDataFormReduxForm = reduxForm({ form: 'edit-profile' })(ProfileDataForm)

export default ProfileDataFormReduxForm;