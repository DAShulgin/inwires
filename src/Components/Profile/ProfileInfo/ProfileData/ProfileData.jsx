import React from 'react';

const ProfileData = ({ profile, isOwner, goToEditMode }) => {


    return (<div>
        {isOwner && <div><button onClick= {goToEditMode}>...Изменить</button></div>}
        <div> <b>Полное имя: </b> {profile.fullName}</div>
        <div> <b>Ищу работу: </b> {profile.lookingForAJob ? 'yes' : 'no'}</div>
        {profile.lookingForAJob &&
            <div> <b>My profession skills: </b> {profile.lookingForAJobDescription}</div>
        }
        <div> <b>About me: </b> {profile.aboutMe}</div>
        <div> <b>Contacts: </b> {Object.keys(profile.contacts).map(key => {
            return <Contact key={key} contactTitle={key} contactValue={profile.contacts[key]} />
        })}
        </div>
    </div>
    )
}
const Contact = ({ contactTitle, contactValue }) => {
    return <div><b>{contactTitle}</b>: {contactValue}</div>
}

export default ProfileData;