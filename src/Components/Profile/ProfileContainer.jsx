import React from 'react';
import { connect } from 'react-redux';
import Profile from './Profile';
import { getUserProfile, getUserStatus, updateUserStatus, savePhoto, saveProfile} from '../../redux/profile-reducer';
import { withRouter } from 'react-router-dom';
import { compose } from '../../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/redux';

class ProfileContainer extends React.Component {

    refreshProfile() {
        let userID = this.props.match.params.userId;

        if (!userID)
        {
            userID = this.props.authrorizedUserId;
            if (!userID) {
                this.props.history.push('/login')
            }
        }
        this.props.getUserProfile(userID);
        this.props.getUserStatus(userID);
    }
    componentDidMount() {
     this.refreshProfile(); 
    }

    componentDidUpdate(prevProps) {
        let userID = this.props.match.params.userId;
        if (userID != prevProps.match.params.userId) { 
        this.refreshProfile(); 
        }
    }

    render() {

        return (
            <div>
                <Profile {...this.props} 
                isOwner={!this.props.match.params.userId}
                profile={this.props.profile}
                status = {this.props.status} 
                update = {this.props.updateUserStatus}
                savePhoto = {this.props.savePhoto}
                saveProfile = {this.props.saveProfile} />

            </div>
        )
    }
}

let mapStateToProps = (state) => ({
    profile: state.profilePage.profile,
    status:  state.profilePage.status,
    authrorizedUserId : state.authorization.userId,
    isAuth: state.authorization.isAuth,
});

export default compose(
connect(mapStateToProps, { getUserProfile, getUserStatus, updateUserStatus, savePhoto, saveProfile }),  
withRouter)(ProfileContainer)

