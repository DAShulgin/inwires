import React from 'react';
import style from './MyPosts.module.css';
import Post from './Post/Post';
import { Field, reduxForm } from 'redux-form'
import { required, maxLengthCreator } from '../../../utils/validators/validator';
import { Textarea } from '../../common/FormControls/FormControls';


const maxLength10 = maxLengthCreator(25); 

const MyPosts =  React.memo(props => {


  let postElements = props.posts
    .map(p => (<Post id={p.id} key={p.id} postText={p.postText} likeCount={p.likeCount} />))


  const addNewPost = (values) => {
    props.addPost(values.TextPost);
  }

  return (
    <div className={style.content}>
      <div className={style.blok1}>
        <div className={style.b_position}>
         <AddPostReduxForm  onSubmit = {addNewPost}/>
        </div>
      </div>
      <div className={style.block2}>
        <div>{postElements}</div>
      </div>
    </div>
  )
});


const AddPostForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div>
      <Field component= {Textarea} name={'TextPost'}   validate={ [required, maxLength10]} placeholder={'Мой пост'} />
      </div>
      <button>Добавить Пост</button>
      <button>Удалить Пост</button>
    </form>
  )
}

const AddPostReduxForm = reduxForm({ form: 'AddPostForm' })(AddPostForm);

export default MyPosts;
