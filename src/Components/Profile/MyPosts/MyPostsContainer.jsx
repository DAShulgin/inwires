import React from 'react';
import { addPostActionCreator} from '../../../redux/profile-reducer';
import MyPosts from './MyPosts';
import { connect } from 'react-redux';


const mapStateToProps = (state) => {
  return {
      posts: state.profilePage.posts,
  }
}
const mapDispacthToProps = (dispatch) => {
  return {
    addPost: (TextPost) => {
          dispatch(addPostActionCreator(TextPost));
      }    
  }
}

const MyPostsContainer = connect(mapStateToProps, mapDispacthToProps)(MyPosts);

export default MyPostsContainer;
