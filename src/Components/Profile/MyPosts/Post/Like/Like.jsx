import React from 'react';
import s from './Like.module.css';
import like from '../../../../../Images/like.png';

const Like = (props) => {
  return (
    <div className={s.item} >
      <span> <img src={like} /> </span>
      <span>{props.likeCount}</span>
    </div>
  )
}
export default Like;
