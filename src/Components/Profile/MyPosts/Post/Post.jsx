import React from 'react';
import style from './Post.module.css';
import notUser from '../../../../Images/notUser.png';
import Like from './Like/Like';


const Post = (props) => {
  return (
 <div>
    <div className={style.item} >
    <img src={notUser} />
     <div className={style.textPost}>{props.postText}</div>
       </div>
    <Like  likeCount = { props.likeCount} />
    </div>
  )
}
export default Post;
