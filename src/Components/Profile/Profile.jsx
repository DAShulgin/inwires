import React from 'react';
import style from './Profile.module.css';
import MyPostsContainer from './MyPosts/MyPostsContainer';
import ProfileInfo from './ProfileInfo/ProfileInfo'


const Profile = (props) => {
  return (
    <div className={style.content}>
      <div className={style.blok1}>
        <ProfileInfo
          savePhoto={props.savePhoto}
          saveProfile={props.saveProfile}
          isOwner={props.isOwner}
          profile={props.profile}
          status={props.status}
          updateUserStatus={props.updateUserStatus}
        />
      </div>
      <div className={style.blok2}> <MyPostsContainer /> </div>
    </div>
  )
}
export default Profile;
