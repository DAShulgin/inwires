import React from 'react';
import style from './FormControls.module.css';
import { Field } from 'redux-form'


export const FormControl = ({ input, meta: {touched, error}, children }) => {

    const Error = touched && error;

    return (
        <div className={style.formControl + ' ' + (Error ? style.error : '')} >
            <div>
                {children}
            </div>
            {Error && <span>{error}</span>}
        </div>
    )
}

export const Textarea = (props) => {
    const { input, meta, child, ...restProps } = props;
    return <FormControl {...props}><textarea {...input} {...restProps} /></FormControl>
}

export const Input = (props) => {
    const { input, meta, child, ...restProps } = props;
    return <FormControl {...props}><input {...input} {...restProps} /></FormControl>
}

export const createField = (placeholder, name, component, validators, props = {}, text ='' ) => (
    <div>
        <Field placeholder={placeholder}
            name={name}
            component={component}
            validate={validators}
            {...props}
        /> {text}
    </div>)
