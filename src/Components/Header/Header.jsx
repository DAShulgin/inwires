import React from 'react';
import styles from './Header.module.css';
import logo from '../../Images/logoMain.png';
import { Redirect } from 'react-router-dom';
import cn from "classnames";


const Header = (props) => {


    return (
        <header className={styles.header}>
            <img src={logo} className={styles.Zimg} />
            <div className={styles.pLine1}><hr color='black' size='2' /></div>
            <div className={styles.pLine2}> <hr color='black' size='2' /> </div>

            <div className={cn({ [styles.loginBlock_auth]: props.isAuth === true }, styles.NotloginBlock_auth)} >

                {props.isAuth ?
                    <div>
                        <div className={styles.auth_name}> {props.login} </div>
                        <div className={styles.auth_photo}></div>
                        <div className={styles.logaut_Btn}> <button onClick={props.logout}> Выйти </button> </div>
    
                    </div> : <Redirect to={'/login'} />}

            </div>
            {/* ---Впроводах-------------  */}
        </header>
    )
}

export default Header;

//   : <NavLink to={'/login'}>Login</NavLink> }