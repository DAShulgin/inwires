import React from 'react';
import Header from './Header';
import { connect } from 'react-redux';
import {getAuthUserData, logout} from '../../redux/authorization-reducer';


class HeaderContainer extends React.Component {

    componentDidMount() {
        this.props.getAuthUserData();
    }
    render() {
        return <Header  {...this.props} />
    }
}
debugger;
let mapStateToProps = (state) => {
    return {
        isAuth: state.authorization.isAuth,
        login: state.authorization.login,
    }

}

export default connect(mapStateToProps,  {getAuthUserData, logout} ) (HeaderContainer); 